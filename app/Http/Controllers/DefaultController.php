<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DefaultController extends Controller
{
    public function services()
    {
        return view('services');
    }

    public function travaux()
    {
        return view('travaux');
    }

    public function contact()
    {
        return view('contact');
    }

    public function storeContact()
    {
        return redirect()->route('contact')->with('success', 'Votre message a bien été envoyé.');
    }

    public function preview(Request $request)
    {
        $content = json_decode($request->getContent(), true);
        return view('admin.preview', ['blocs' => $content]);
    }
}
