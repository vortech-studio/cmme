<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function create()
    {
        return view('admin.page.create');
    }

    public function store(Request $request)
    {
        $page = Page::create([
            'title' => $request->get('title'),
            "contenue" => $request->get('contenue'),
            "parent" => $request->get('parent_id') !== '',
            "parent_id" => $request->get('parent_id') !== '' ? $request->get('parent_id') : null,
            'published' => $request->has('published'),
        ]);

        return redirect()->route('dashboard');
    }
}
