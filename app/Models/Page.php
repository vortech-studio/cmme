<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $guarded = [];

    public function getContenueAttribute()
    {
        return json_decode($this->attributes['contenue']);
    }
}
