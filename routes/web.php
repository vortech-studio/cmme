<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('index');
})->name('home');

Route::get('/services', [\App\Http\Controllers\DefaultController::class, 'services'])->name('services');
Route::get('/travaux', [\App\Http\Controllers\DefaultController::class, 'travaux'])->name('travaux');
Route::get('/contact', [\App\Http\Controllers\DefaultController::class, 'contact'])->name('contact');
Route::post('/contact', [\App\Http\Controllers\DefaultController::class, 'storeContact'])->name('storeContact');
Route::post('/preview', [\App\Http\Controllers\DefaultController::class, 'preview'])->name('preview')->withoutMiddleware([\App\Http\Middleware\VerifyCsrfToken::class]);

Route::prefix('admin')->middleware(['auth'])->group(function () {
    Route::get('/', [\App\Http\Controllers\Admin\DashboardController::class, 'index'])->name('dashboard');
    Route::prefix('pages')->group(function () {
        Route::get('create', [\App\Http\Controllers\Admin\PageController::class, 'create'])->name('page.create');
        Route::post('create', [\App\Http\Controllers\Admin\PageController::class, 'store'])->name('page.store');
    });
});


require __DIR__.'/auth.php';
