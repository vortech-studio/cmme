import 'uikit/dist/js/uikit.min.js'
import 'uikit/dist/js/uikit-icons.min.js'
import 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/js/all.min.js'
import 'jstree/dist/themes/default/style.min.css'
import 'jstree/dist/jstree.min.js'
import '../../scss/styles.scss'

import Alpine from 'alpinejs';

window.Alpine = Alpine;

Alpine.start();
