@extends("template")

@section("css")

@endsection

@section("content")
    <!-- BEGIN: PAGE CONTENT -->
    <!-- BEGIN: LAYOUT/SLIDERS/REVO-SLIDER-4 -->
    <section class="c-layout-revo-slider c-layout-revo-slider-4" dir="ltr">
        <div class="tp-banner-container c-theme">
            <div class="tp-banner rev_slider" data-version="5.0">
                <ul>
                    <!--BEGIN: SLIDE #1 -->
                    <li data-transition="fade" data-slotamount="1" data-masterspeed="1000">
                        <img
                            alt=""
                            src="https://www.charpentemetallique-bobet.fr/media/3990/big/charpente-ossature-metallique.jpg"
                            data-bgposition="center center"
                            data-bgfit="cover"
                            data-bgrepeat="no-repeat"
                        >
                        <div class="tp-caption customin customout"
                             data-x="center"
                             data-y="center"
                             data-hoffset=""
                             data-voffset="-50"
                             data-speed="500"
                             data-start="1000"
                             data-transform_idle="o:1;"
                             data-transform_in="rX:0.5;scaleX:0.75;scaleY:0.75;o:0;s:500;e:Back.easeInOut;"
                             data-transform_out="rX:0.5;scaleX:0.75;scaleY:0.75;o:0;s:500;e:Back.easeInOut;"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.1"
                             data-endelementdelay="0.1"
                             data-endspeed="600">
                            <h3 class="c-main-title-circle c-font-48 c-font-bold c-font-center c-font-uppercase c-font-white c-block">
                                Bienvenue sur le site de<br> CMME Construction
                            </h3>
                        </div>
                    </li>
                    <!--END -->
                </ul>
            </div>
        </div>
    </section><!-- END: LAYOUT/SLIDERS/REVO-SLIDER-4 -->
    <div class="c-content-media-1 c-bordered wow  fadeInLeft animated text-center" style="min-height: 380px; visibility: visible; animation-name: fadeInLeft; opacity: 1;">
        <div class="c-content-label c-font-uppercase c-font-bold c-theme-bg">Notre mission</div>
        <a href="#" class="c-title c-font-uppercase c-theme-on-hover c-font-bold">Structure métallique durable</a>
        <p>Chez CMME, notre engagement est de fournir des solutions de pose de toiture et de structures métalliques de la plus haute qualité. Nous sommes animés par la passion de créer des espaces sûrs, durables et esthétiquement agréables, tout en surpassant les attentes de nos clients. Notre équipe de professionnels expérimentés et dévoués s'efforce continuellement d'offrir un service exceptionnel, alliant expertise technique et excellence artisanale.</p>
    </div>
@endsection

@section("script")

@endsection
