<div class="c-content-box c-size-md c-no-padding c-bg-white">
    @dd($bloc)
    <div class="c-content-feature-4">
        <div class="c-bg-parallax c-feature-bg c-content-right c-diagonal c-border-left-white" style="background-image: url({{ $bloc['background'] }})"></div>
        <div class="c-content-area c-content-left"></div>
        <div class="container">
            <div class="c-feature-content c-left">
                <div class="c-content-v-center">
                    <div class="c-wrapper">
                        <div class="c-body">
                            <div class="c-content-title-1 wow animate fadeInDown" style="visibility: visible; animation-name: fadeInDown; opacity: 1;">
                                <h3 class="c-font-uppercase c-font-bold c-left">{{ $bloc['title'] }}</h3>
                                <div class="c-line-left"></div>
                                <p class="c-left">{{ $bloc['subtitle'] }}</p>
                            </div>
                            <div class="c-content">
                                <p class="c-margin-b-30 c-left wow animate fadeInDown" style="visibility: visible; animation-name: fadeInDown; opacity: 1;">
                                    {!! $bloc['content'] !!}
                                </p>
                                @foreach($bloc['buttons'] as $button)
                                    <a href="{{ $button['url'] }}" class="btn btn-lg c-btn-{{ $button['type'] }} c-btn-border-2x c-btn-square c-btn-bold wow animate fadeIn" style="visibility: visible; animation-name: fadeIn; opacity: 1;">{{ $button['label'] }}</a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
