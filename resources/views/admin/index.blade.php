@extends("admin.template")

@section("content")
    <div class="container bg-grey-300">
        <div class="card shadow-lg">
            <div class="card-header">
                <div class="card-title">Gestion des pages</div>
                <div class="card-toolbar">
                    <a href="{{ route('page.create') }}" class="btn btn-primary"><i class="fa-solid fa-plus-circle"></i> Nouvelle page</a>
                </div>
            </div>
            <div class="card-body">
                <table class="uk-table uk-table-striped uk-table-middle">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Page</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach(\App\Models\Page::all() as $page)
                            <tr>
                                <td>{{ $page->id }}</td>
                                <td>
                                    @if(!$page->parent)
                                        {{ $page->title }}
                                    @else
                                        <span class="ms-5 fst-italic fs-6">{{ $page->title }}</span>
                                    @endif
                                </td>
                                <td>
                                    <a href="" class="btn btn-sm btn-secondary"><i class="fa-solid fa-edit"></i> Editer</a>
                                    <form action="">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-sm btn-danger" type="submit"><i class="fa-solid fa-trash"></i> Supprimer</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section("scripts")
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.7.1/dist/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        document.addEventListener('DOMContentLoaded', () => {
            $("#treePage").jstree();
        })
    </script>
@endsection
