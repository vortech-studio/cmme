<!doctype html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('app.name') }} - Administration</title>
    <link href="//cdn.quilljs.com/1.3.6/quill.core.css" rel="stylesheet">
    <link href="//cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
    <link href="//cdn.quilljs.com/1.3.6/quill.bubble.css" rel="stylesheet">
    @vite(['resources/css/admin/app.css'])
    @yield('styles')
</head>
<body>
<header class="text-bg-dark">
    <div class="container d-flex flex-wrap justify-content-center py-3 mb-4 border-bottom">
        <a href="/" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto link-body-emphasis text-decoration-none">
            <img src="{{ asset('/storage/logo.jpg') }}" alt="Logo" class="w-70px">
        </a>
    </div>
</header>
@yield("content")
<script src="//cdn.quilljs.com/1.3.6/quill.min.js"></script>

@vite(['resources/js/admin/app.js'])
@yield("scripts")
</body>
</html>
