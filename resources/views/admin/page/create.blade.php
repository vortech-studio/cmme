@extends("admin.template")

@section("styles")
@endsection

@section("content")
    <div class="container bg-grey-300">
        <div class="card shadow-lg">
            <div class="card-header">
                <div class="card-title">Nouvelle Page</div>

            </div>
            <form action="{{ route('page.store') }}" method="post">
                @csrf
                <div class="card-body">
                    <div class="uk-margin">
                        <input class="uk-input" type="text" name="title" placeholder="Titre de la page" aria-label="Input">
                    </div>
                    <div class="uk-margin">
                        <select class="uk-select" name="parent_id" aria-label="Page Parente">
                            <option value="">Page Parente</option>
                            @foreach(\App\Models\Page::all() as $page)
                                <option value="{{ $page->id }}">{{ $page->title }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="uk-margin uk-grid-small uk-child-width-auto uk-grid">
                        <label><input class="uk-checkbox" name="published" value="1" type="checkbox" checked> Publié</label>
                    </div>
                    <div class="uk-margin" x-data="{open: false}">
                        <button class="btn btn-primary" type="button" @click="open = true">Afficher l'éditeur</button>
                        <visual-editor
                            :hidden="open === false"
                            @close="open = false"
                            name="contenue"
                            preview="{{ route('preview') }}"
                            iconUrl="{{ asset('/storage/editor/[name].png') }}"
                        ></visual-editor>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="d-flex flex-row justify-content-end">
                        <button class="btn btn-primary" type="submit">Enregistrer</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section("scripts")
    <script type="module">
        import {VisualEditor, Text, HTMLText, Repeater, Row, Select, ImageUrl} from 'https://www.unpkg.com/@boxraiser/visual-editor@0.1.0/VisualEditor.standalone.js';
        const editor = new VisualEditor()
        editor.defineElement()
        editor.registerComponent('parallax_type_1', {
            title: 'Parallax Type 1',
            category: 'Parallax',
            fields: [
                Text('title', {multiline: false, label: 'Titre'}),
                Text('subtitle', {multiline: false, label: 'Sous Titre'}),
                ImageUrl('background', {label: 'Image de fond'}),
                HTMLText('content', {label: 'Contenu'}),
                Repeater('buttons', {
                    title: 'Boutons',
                    addLabel: 'Ajouter un bouton',
                    fields: [
                        Row([
                            Text('label', { label: 'Label', default: 'Call to action' }),
                            Text('url', { label: 'Link' }),
                            Select('type', {
                                default: 'primary',
                                label: 'type',
                                options: [
                                    { label: 'Primaire', value: 'primary' },
                                    { label: 'Secondaire', value: 'secondary' },
                                    { label: 'Danger', value: 'danger' },
                                    { label: 'Attention', value: 'warning' },
                                    { label: 'Succès', value: 'success' },
                                    { label: 'Info', value: 'info' },
                                ]
                            })
                        ])
                    ]
                })
            ]
        })
    </script>
@endsection
