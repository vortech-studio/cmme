@extends("template")

@section("css")

@endsection

@section("content")
    <div id="ve-components">
        @foreach($blocs as $bloc)
            @include("blocs.".$bloc['_name'], $bloc)
        @endforeach
    </div>
@endsection

@section("script")

@endsection
