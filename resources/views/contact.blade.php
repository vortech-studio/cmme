@extends("template")

@section("css")

@endsection

@section("content")
    <div class="c-layout-breadcrumbs-1 c-bgimage-full   c-fonts-uppercase c-fonts-bold   c-bg-img-center" style="background-image: url(../../assets/base/img/content/backgrounds/bg-18.jpg)">
        <div class="c-breadcrumbs-wrapper">
            <div class="container">
                <div class="c-page-title c-pull-left">
                    <h3 class="c-font-uppercase c-font-bold c-font-white c-font-20 c-font-slim c-opacity-09">Nous contacter</h3>
                    <h4 class="c-font-white c-font-thin c-opacity-09"></h4>
                </div>
                <ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
                    <li><a href="#" class="c-font-white">Acceuil</a></li>
                    <li class="c-font-white">/</li>
                    <li class="c-state_active c-font-white">Nous contacter</li>

                </ul>
            </div>
        </div>
    </div>
    <div class="c-content-box c-size-md c-bg-white">
        <div class="container">
            <div class="c-content-feedback-1 c-option-1">
                <div class="row">
                    <div class="col-md-12">
                        <div class="c-contact">
                            <div class="c-content-title-1">
                                <h3 class="c-font-uppercase c-font-bold">Nous contacter</h3>
                                <div class="c-line-left"></div>
                                <p class="c-font-lowercase">N'hésitez pas à nous envoyer un e-mail à partir du formulaire ci-dessous et nous vous répondrons dans les plus brefs délais.</p>
                            </div>
                            <form action="#">
                                <div class="form-group">
                                    <input type="text" placeholder="Votre Société" class="form-control c-square c-theme input-lg">
                                </div>
                                <div class="form-group">
                                    <input type="text" placeholder="Votre Nom / Prénom" class="form-control c-square c-theme input-lg">
                                </div>
                                <div class="form-group">
                                    <input type="text" placeholder="Votre email" class="form-control c-square c-theme input-lg">
                                </div>
                                <div class="form-group">
                                    <input type="text" placeholder="Votre téléphone" class="form-control c-square c-theme input-lg">
                                </div>
                                <div class="form-group">
                                    <textarea rows="8" name="message" placeholder="Ecrire un message..." class="form-control c-theme c-square input-lg"></textarea>
                                </div>
                                <button type="submit" class="btn c-theme-btn c-btn-uppercase btn-lg c-btn-bold c-btn-square">Envoyer un message</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDLPWnOu970nrxAY1eApfQb0UKBaxov3zg" type="text/javascript"></script>
    <script src="/assets/plugins/gmaps/gmaps.js" type="text/javascript"></script>
    <script type="text/javascript">
        var PageContact = function() {

            var _init = function() {

                var mapbg = new GMaps({
                    div: '#gmapbg',
                    lat: 3.118823,
                    lng: 101.676084,
                    scrollwheel: false,
                });


                mapbg.addMarker({
                    lat: 3.118823,
                    lng: 101.676084,
                    title: 'Your Location',
                    infoWindow: {
                        content: '<h3>Jango Inc.</h3><p>25, Lorem Lis Street, Orange C, California, US</p>'
                    }
                });
            }

            return {
                //main function to initiate the module
                init: function() {

                    _init();

                }

            };
        }();

        $(document).ready(function() {
            PageContact.init();
            $( window ).resize(function() {
                PageContact.init();
            });
        });
    </script>
@endsection
