@extends("template")

@section("css")

@endsection

@section("content")
    <div class="c-layout-breadcrumbs-1 c-bgimage c-subtitle c-fonts-uppercase c-fonts-bold c-bg-img-center-contain" style="background-image: url({{ asset('/storage/charpente.jpg') }})">
        <div class="container">
            <div class="c-page-title c-pull-left">
                <h3 class="c-font-uppercase c-font-bold c-font-white c-font-20 c-font-slim">Nos services</h3>
                <h4 class="c-font-white c-font-thin c-opacity-07">
                    En quoi pouvons-nous vous aider ?
                </h4>
            </div>
            <ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
                <li><a href="#" class="c-font-white">Acceuil</a></li>
                <li class="c-font-white">/</li>
                <li class="c-state_active c-font-white">Nos services</li>

            </ul>
        </div>
    </div>
    <div class="c-content-box c-size-md c-bg-white">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="c-content-media-1 c-bordered wow  fadeInLeft animated" style="min-height: 380px; visibility: visible; animation-name: fadeInLeft; opacity: 1;">
                        <div class="c-content-label c-font-uppercase c-font-bold c-theme-bg">Notre mission</div>
                        <a href="#" class="c-title c-font-uppercase c-theme-on-hover c-font-bold">Structure métallique durable</a>
                        <p>Chez CMME, notre engagement est de fournir des solutions de pose de toiture et de structures métalliques de la plus haute qualité. Nous sommes animés par la passion de créer des espaces sûrs, durables et esthétiquement agréables, tout en surpassant les attentes de nos clients. Notre équipe de professionnels expérimentés et dévoués s'efforce continuellement d'offrir un service exceptionnel, alliant expertise technique et excellence artisanale.</p>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="c-content-media-2-slider wow  fadeInRight animated" data-slider="owl" style="visibility: visible; animation-name: fadeInRight; opacity: 1;">
                        <div class="c-content-label c-font-uppercase c-font-bold">Derniers Travaux</div>
                        <div class="owl-carousel owl-theme c-theme owl-single owl-loaded" data-single-item="true" data-navigation-dots="true" data-auto-play="4000" data-rtl="false">



                            <div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(-3750px, 0px, 0px); transition: all 0.25s ease 0s; width: 5250px;"><div class="owl-item cloned" style="width: 750px; margin-right: 0px;"><div class="c-content-media-2 c-bg-img-center" style="background-image: url(http://via.placeholder.com/640x360); min-height: 380px;">
                                            <div class="c-panel">
                                                <div class="c-fav">
                                                    <i class="icon-heart c-font-thin"></i>
                                                    <p class="c-font-thin">24</p>
                                                </div>
                                            </div>
                                        </div></div><div class="owl-item cloned" style="width: 750px; margin-right: 0px;"><div class="c-content-media-2 c-bg-img-center" style="background-image: url(http://via.placeholder.com/640x360); min-height: 380px;">
                                            <div class="c-panel">
                                                <div class="c-fav">
                                                    <i class="icon-heart c-font-thin"></i>
                                                    <p class="c-font-thin">19</p>
                                                </div>
                                            </div>
                                        </div></div><div class="owl-item" style="width: 750px; margin-right: 0px;"><div class="c-content-media-2 c-bg-img-center" style="background-image: url(http://via.placeholder.com/640x360); min-height: 380px;">
                                            <div class="c-panel">
                                                <div class="c-fav">
                                                    <i class="icon-heart c-font-thin"></i>
                                                    <p class="c-font-thin">16</p>
                                                </div>
                                            </div>
                                        </div></div><div class="owl-item" style="width: 750px; margin-right: 0px;"><div class="c-content-media-2 c-bg-img-center" style="background-image: url(http://via.placeholder.com/640x360); min-height: 380px;">
                                            <div class="c-panel">
                                                <div class="c-fav">
                                                    <i class="icon-heart c-font-thin"></i>
                                                    <p class="c-font-thin">24</p>
                                                </div>
                                            </div>
                                        </div></div><div class="owl-item" style="width: 750px; margin-right: 0px;"><div class="c-content-media-2 c-bg-img-center" style="background-image: url(http://via.placeholder.com/640x360); min-height: 380px;">
                                            <div class="c-panel">
                                                <div class="c-fav">
                                                    <i class="icon-heart c-font-thin"></i>
                                                    <p class="c-font-thin">19</p>
                                                </div>
                                            </div>
                                        </div></div><div class="owl-item cloned active" style="width: 750px; margin-right: 0px;"><div class="c-content-media-2 c-bg-img-center" style="background-image: url(http://via.placeholder.com/640x360); min-height: 380px;">
                                            <div class="c-panel">
                                                <div class="c-fav">
                                                    <i class="icon-heart c-font-thin"></i>
                                                    <p class="c-font-thin">16</p>
                                                </div>
                                            </div>
                                        </div></div><div class="owl-item cloned" style="width: 750px; margin-right: 0px;"><div class="c-content-media-2 c-bg-img-center" style="background-image: url(http://via.placeholder.com/640x360); min-height: 380px;">
                                            <div class="c-panel">
                                                <div class="c-fav">
                                                    <i class="icon-heart c-font-thin"></i>
                                                    <p class="c-font-thin">24</p>
                                                </div>
                                            </div>
                                        </div></div></div></div><div class="owl-controls"><div class="owl-nav"><div class="owl-prev" style="display: none;"></div><div class="owl-next" style="display: none;"></div></div><div class="owl-dots" style=""><div class="owl-dot active"><span></span></div><div class="owl-dot"><span></span></div><div class="owl-dot"><span></span></div></div></div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="c-content-box c-size-md c-bg-white">
        <div class="container">
            <div class="c-content-feature-2-grid">
                <div class="c-content-title-1 wow  fadeIn animated" style="visibility: visible; animation-name: fadeIn; opacity: 1;">
                    <h3 class="c-font-uppercase c-font-bold">Services que nous effectuons</h3>
                    <div class="c-line-left"></div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-6">
                        <div class="c-content-feature-2 c-option-2 c-theme-bg-parent-hover wow  fadeInUp animated" style="visibility: visible; animation-name: fadeInUp; opacity: 1;">
                            <div class="c-icon-wrapper c-theme-bg-on-parent-hover">
                                <div class="c-content-line-icon c-theme c-icon-screen-chart"></div>
                            </div>
                            <h3 class="c-font-uppercase c-title">Bureau d'étude</h3>
                            <p>Nous réalisons les études allans des esquisses aux plans de réalisations et d'assemblage</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="c-content-feature-2 c-option-2 c-theme-bg-parent-hover wow  fadeInUp animated" style="visibility: visible; animation-name: fadeInUp; opacity: 1;">
                            <div class="c-icon-wrapper c-theme-bg-on-parent-hover">
                                <div class="c-content-line-icon c-theme c-icon-support"></div>
                            </div>
                            <h3 class="c-font-uppercase c-title">Génie Civil</h3>
                            <p>Nous effectuons des travaux de terrassement pour préparer le terrain avant la construction.<br>Notre équipement moderne nous permet d'accomplir ces tâches de manières efficace et précise.</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="c-content-feature-2 c-option-2 c-theme-bg-parent-hover wow  fadeInUp animated" style="visibility: visible; animation-name: fadeInUp; opacity: 1;">
                            <div class="c-icon-wrapper c-theme-bg-on-parent-hover">
                                <div class="c-content-line-icon c-theme c-icon-comment"></div>
                            </div>
                            <h3 class="c-font-uppercase c-title">Maçonnerie</h3>
                            <p>Nous réalisons tous types de travaux de maçonnerie, de gros oeuvres aux finitions minutieuses. Nos maçons qualifiés garantissent des constructions solides et durables.</p>
                            <ul>
                                <li>Structure</li>
                                <li>Mur de soutènement branchés</li>
                                <li>Construction</li>
                                <li>Rénovation</li>
                                <li>Assainissement</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="c-content-feature-2 c-option-2 c-theme-bg-parent-hover wow  fadeInUp animated" style="visibility: visible; animation-name: fadeInUp; opacity: 1;">
                            <div class="c-icon-wrapper c-theme-bg-on-parent-hover">
                                <div class="c-content-line-icon c-theme c-icon-bulb"></div>
                            </div>
                            <h3 class="c-font-uppercase c-title">Mennuiseries métalliques</h3>
                            <p>Nos menuisiers métalliques créent des structures métalliques sur mesure pour vos batiments, assurant ainsi à la fois esthétisme et sécurité</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="c-content-feature-2 c-option-2 c-theme-bg-parent-hover wow  fadeInUp animated" style="visibility: visible; animation-name: fadeInUp; opacity: 1;">
                            <div class="c-icon-wrapper c-theme-bg-on-parent-hover">
                                <div class="c-content-line-icon c-theme c-icon-sticker"></div>
                            </div>
                            <h3 class="c-font-uppercase c-title">Installations de charpentes</h3>
                            <p>Nous installons des charpentes de haute qualité, en utilisant des matériaux durables et en respectant les normes de construction les plus strictes.</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="c-content-feature-2 c-option-2 c-theme-bg-parent-hover wow  fadeInUp animated" style="visibility: visible; animation-name: fadeInUp; opacity: 1;">
                            <div class="c-icon-wrapper c-theme-bg-on-parent-hover">
                                <div class="c-content-line-icon c-theme c-icon-globe"></div>
                            </div>
                            <h3 class="c-font-uppercase c-title">Serrureries</h3>
                            <p>Nos serruriers expériementés vous fournissent des solutions de sécurité fiables pour vos batiments. Que ce soit pour des portes, des fenêtres ou des systèmes de verrouillage, nous avons la solution adaptée à vos besoins.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="c-content-box c-size-md c-bg-white">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 wow  fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft; opacity: 1;">
                    <!-- Begin: Title 1 component -->
                    <div class="c-content-title-1">
                        <h3 class="c-font-uppercase c-font-bold">A propos de nous</h3>
                        <div class="c-line-left c-theme-bg"></div>
                    </div>
                    <!-- End-->
                    <p>Nos équipes sont résolument tournés vers le marché des energies renouvelables. Nous avaons développé une expertise dans la construction de batiment capable d'accueillir des centrales photovoltaïques sur leurs toitures.<br>Contribuez à un avenir durable en optant pour nos solutions de construction respectueuse de l'environnement.</p>
                </div>
                <div class="col-sm-6 wow  fadeInRight animated" style="visibility: visible; animation-name: fadeInRight; opacity: 1;">
                    <div class="c-content-client-logos-1">
                        <!-- Begin: Title 1 component -->
                        <div class="c-content-title-1">
                            <h3 class="c-font-uppercase c-font-bold">Nos clients</h3>
                            <div class="c-line-left c-theme-bg"></div>
                        </div>
                        <!-- End-->
                        <!--<div class="c-logos">
                            <div class="row">
                                <div class="col-md-4 col-xs-6 c-logo c-logo-1">
                                    <a href="#"><img class="c-img-pos" src="../../assets/base/img/content/client-logos/client1.jpg" alt=""></a>
                                </div>
                                <div class="col-md-4 col-xs-6 c-logo c-logo-2">
                                    <a href="#"><img class="c-img-pos" src="../../assets/base/img/content/client-logos/client2.jpg" alt=""></a>
                                </div>
                                <div class="col-md-4 col-xs-6 c-logo c-logo-3">
                                    <a href="#"><img class="c-img-pos" src="../../assets/base/img/content/client-logos/client3.jpg" alt=""></a>
                                </div>
                                <div class="col-md-4 col-xs-6 c-logo c-logo-4">
                                    <a href="#"><img class="c-img-pos" src="../../assets/base/img/content/client-logos/client4.jpg" alt=""></a>
                                </div>
                                <div class="col-md-4 col-xs-6 c-logo c-logo-5">
                                    <a href="#"><img class="c-img-pos" src="../../assets/base/img/content/client-logos/client5.jpg" alt=""></a>
                                </div>
                                <div class="col-md-4 col-xs-6 c-logo c-logo-6">
                                    <a href="#"><img class="c-img-pos" src="../../assets/base/img/content/client-logos/client6.jpg" alt=""></a>
                                </div>
                            </div>
                        </div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section("script")

@endsection
